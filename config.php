<?php

return [
    'db' => [
        'driver' => env('DB_DRIVER', 'mysql'),
        'host' => env('DB_HOST', 'localhost'),
        'port' => env('DB_PORT'),
        'database' => env('DB_NAME', 'default'),
        'username' => 'homestead',
        'password' => 'secret',
    ],
];
