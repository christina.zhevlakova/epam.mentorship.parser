create table films (
  id serial primary key,
  title varchar(100) not null,
  year smallint not null default 0,
  poster varchar(1024) not null default '',
  country varchar(1024) not null default '',
  story text not null default '',
  UNIQUE (title, year)
);

create table personalities (
  id serial primary key,
  name varchar(100) not null unique
);

create table film_personalities (
  film_id integer not null references films (id) ON update cascade on delete cascade,
  personality_id integer not null references personalities (id) ON update cascade on delete restrict,
  role varchar(64) not null,
  primary key (film_id, personality_id, role)
);