<?php

if(!function_exists('env')) {
    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    function env(string $name, $default=null)
    {
        return $_ENV[$name] ?? $default;
    }
}
