<?php

require '../vendor/autoload.php';

(new \Dotenv\Dotenv(dirname(__DIR__)))->load();

$domains = [
    'imdb' => [
        'https://www.imdb.com/title/tt6494418/',
        'https://www.imdb.com/title/tt2837574/',
        'https://www.imdb.com/title/tt4154756/',
        'https://www.imdb.com/title/tt5814060/',
        'https://www.imdb.com/title/tt4779682/'
    ]
];

try {
    $films = [];

    foreach($domains as $domain=>$urls) {
        $parser = \App\Parsers\ParserFactory::init($domain);

        foreach($urls as $url) {
            try{
                $films[] = (new \App\HandleUrlLoggingDecorator(
                    new \App\HandleUrl(
                        new \App\Http\GuzzleAdapter())
                ))->run($parser, $url);
            }
            catch (\App\Exceptions\FilmNotFoundException $e) {
                continue;
            }
        }
    }
}
catch(\Exception $e) {
    echo 'Found exception: '.$e->getMessage();
    exit;
}
echo 'Success. Added ' . count($films) . ' urls';

