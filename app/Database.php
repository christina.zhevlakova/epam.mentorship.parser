<?php

namespace App;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;

/**
 * Class Database
 * @package App
 */
class Database
{
    /**
     * @var
     */
    private static $instance;

    /**
     * @var Manager
     */
    private $manager;

    /**
     * Logger constructor.
     */
    private function __construct()
    {
        $this->manager = new Manager;

        $this->manager->addConnection(Config::getInstance()->get('db'));
        $this->manager->setAsGlobal();
        $this->manager->bootEloquent();
    }

    /**
     *
     */
    private function __clone() {}

    /**
     * @return Database
     */
    public static function getInstance(): Database
    {
        if(!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @return DatabaseManager
     */
    public function getManager(): DatabaseManager
    {
        return $this->manager->getDatabaseManager();
    }

    /**
     * @param string|null $name
     * @return Connection
     */
    public function getConnection(string $name = null): Connection
    {
        return $this->manager->getConnection($name);
    }
}