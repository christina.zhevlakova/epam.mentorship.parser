<?php

namespace App\Parsers;

use App\Models\Film;
use App\Models\Personality;
use DiDom\Document;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ImdbParser
 * @package App\Parsers
 */
class ImdbParser implements ParserInterface
{
    /**
     * @var array
     */
    private $selectors = [
        'title' => 'div.title_wrapper > h1',
        'persons' => 'div.credit_summary_item',
        'story' => '#titleStoryLine p',
        'poster' => '.poster img',
        'year' => 'div.title_wrapper > h1 a',
        'country' => '#titleDetails div.txt-block',
    ];

    private $exceptPersons = [
        Film::ROLE_DIRECTOR => [],
        Film::ROLE_ACTOR => ['See full cast & crew'],
    ];

    /**
     * @param Document $document
     * @return Film
     */
    public function parse(Document $document): Film
    {
        $film = new Film();
        $film->title = trim($document->find($this->selectors['title'])[0]->text());
        $film->year = (int)trim($document->find($this->selectors['year'])[0]->text());
        $film->country = $this->parseCountry($document);
        $film->story = trim($document->find($this->selectors['story'])[0]->text());
        $film->poster = trim($document->find($this->selectors['poster'])[0]->attr('src'));
        $film->personality = $this->parsePersons($document);

        return $film;
    }

    /**
     * @param Document $document
     * @return Collection
     */
    private function parsePersons(Document $document): Collection
    {
        $personalities = new Collection();

        $persons = $document->find($this->selectors['persons']);

        foreach($persons[0]->find('a') as $item) {
            $name = trim($item->text());

            if(in_array($name, $this->exceptPersons[Film::ROLE_DIRECTOR])
                || $personalities
                    ->where('name', $name)
                    ->where('role', Film::ROLE_DIRECTOR)
                    ->count()
            ) {
                continue;
            }

            $personality = new Personality(['name' => $name]);
            $personality->role = Film::ROLE_DIRECTOR;
            $personalities->push($personality);

        }

        foreach($persons[2]->find('a') as $item) {
            $name = trim($item->text());

            if(in_array($name, $this->exceptPersons[Film::ROLE_ACTOR])
                || $personalities
                    ->where('role', Film::ROLE_ACTOR)
                    ->where('name', $name)
                    ->count()
            ) {
                continue;
            }

            $personality = new Personality(['name' => $name]);
            $personality->role = Film::ROLE_ACTOR;
            $personalities->push($personality);
        }

        return $personalities;
    }

    /**
     * @param Document $document
     * @return string
     */
    private function parseCountry(Document $document): string
    {
        $countries = [];

        foreach($document->find($this->selectors['country'])[1]->find('a') as $item) {
            $countries[] = trim($item->text());
        }

        return implode(', ', $countries);
    }
}
