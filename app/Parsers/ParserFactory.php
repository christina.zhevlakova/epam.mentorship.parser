<?php

namespace App\Parsers;

use App\Exceptions\InvalidDataException;

/**
 * Class ParserFactory
 * @package App\Parsers
 */
class ParserFactory
{
    /**
     * @param string $type
     *
     * @return \App\Parsers\ParserInterface
     * @throws \App\Exceptions\InvalidDataException
     */
    public static function init(string $type): ParserInterface
    {
        switch($type) {
            case 'imdb':
                return new ImdbParser;
            default:
                throw new InvalidDataException;
        }
    }
}