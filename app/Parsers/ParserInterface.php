<?php

namespace App\Parsers;

use App\Models\Film;
use DiDom\Document;

/**
 * Interface ParserInterface
 * @package App\Parsers
 */
interface ParserInterface
{
    /**
     * @param Document $document
     * @return Film
     */
    public function parse(Document $document): Film;
}
