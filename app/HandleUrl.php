<?php

namespace App;

use App\Exceptions\FilmNotFoundException;
use App\Http\HttpAdapterInterface;
use App\Models\Film;
use App\Models\Personality;
use App\Parsers\ParserInterface;
use DiDom\Document;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class HandleUrl
 * @package App
 */
class HandleUrl implements HandleUrlInterface
{
    /**
     * @var HttpAdapterInterface
     */
    private $httpAdapter;

    /**
     * HandleUrl constructor.
     * @param HttpAdapterInterface $httpAdapter
     */
    public function __construct(HttpAdapterInterface $httpAdapter)
    {
        $this->httpAdapter = $httpAdapter;

        Database::getInstance();
    }

    /**
     * @param ParserInterface $parser
     * @param string $url
     * @return Film
     * @throws FilmNotFoundException
     * @throws \Exception
     */
    public function run(ParserInterface $parser, string $url): Film
    {
        $film = $this->parseUrl($parser, $url);
        $personalities = $this->savePersonalities($film->personality);

        if($dbFilm = Film::where([
            ['title', $film->title],
            ['year', $film->year]
        ])->first()) {
            $film->id = $dbFilm->id;
        }
        else {
            $film->save();
        }

        $personIds = [];

        foreach($personalities as $personality) {
            $personIds[$personality->id] = ['role' => $personality->role];
        }

        $film->personalities()->sync($personIds);

        return $film;
    }

    /**
     * @param ParserInterface $parser
     * @param string $url
     * @return Film
     * @throws FilmNotFoundException
     */
    private function parseUrl(ParserInterface $parser, string $url): Film
    {
        try{
            $content = $this->httpAdapter->getContent($url);
        }
        catch (\Exception $e) {
            throw new FilmNotFoundException;
        }
        return $parser->parse(new Document($content));
    }

    /**
     * @param Collection $personalities
     * @return Collection
     */
    private function savePersonalities(Collection $personalities): Collection
    {
        foreach ($personalities as $personality) {
            if($person = Personality::where('name', $personality->name)->first()) {
                $personality->id = $person->id;
            }
            else {
                $personality->save();
            }
        }

        return $personalities;
    }
}
