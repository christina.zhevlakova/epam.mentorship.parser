<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Personality
 * @package App\Models
 */
class Personality extends Model
{
    /**
     * @var string
     */
    protected $table = 'personalities';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    public $role = '';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function films()
    {
        return $this->belongsToMany(
            Film::class,
            'film_personalities'
        )->withPivot('role');
    }
}