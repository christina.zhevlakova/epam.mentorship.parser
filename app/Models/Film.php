<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Film
 * @package App\Models
 */
class Film extends Model
{
    const ROLE_DIRECTOR = 'directors';
    const ROLE_ACTOR = 'actors';
    const ALLOWED_ROLES = [
        self::ROLE_DIRECTOR,
        self::ROLE_ACTOR,
    ];

    /**
     * @var string
     */
    protected $table = 'films';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'poster',
        'country',
        'story',
        'year',
    ];

    /**
     * @var Collection
     */
    public $personality;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function personalities()
    {
        return $this->belongsToMany(
            Personality::class,
            'film_personalities'
        )->withPivot('role');
    }
}
