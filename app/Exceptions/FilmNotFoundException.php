<?php

namespace App\Exceptions;

/**
 * Class FilmNotFoundException
 * @package App\Exceptions
 */
class FilmNotFoundException extends \Exception
{
    protected $message = 'Film not found';
    protected $code = 400;
}