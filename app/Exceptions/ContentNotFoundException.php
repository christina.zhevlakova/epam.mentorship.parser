<?php

namespace App\Exceptions;

/**
 * Class FilmNotFoundException
 * @package App\Exceptions
 */
class ContentNotFoundException extends \Exception
{
    protected $message = 'Content not found';
    protected $code = 400;
}