<?php

namespace App\Exceptions;

/**
 * Class InvalidDataException
 * @package App\Exceptions
 */
class InvalidDataException extends \Exception
{
    protected $message = 'Invalid data';
    protected $code = 400;
}