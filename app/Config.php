<?php

namespace App;

/**
 * Class Logger
 * @package App
 */
class Config
{
    /**
     * @var
     */
    private static $instance;

    /**
     * @var array|mixed
     */
    private $config = [];

    /**
     * Logger constructor.
     */
    private function __construct()
    {
        $this->config = require_once dirname(__DIR__).'/config.php';
    }

    /**
     *
     */
    private function __clone() {}

    /**
     * @return Config
     */
    public static function getInstance(): Config
    {
        if(!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->config;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public function get(string $key, $default=null)
    {
        return $key
            ? (key_exists($key, $this->config) ? $this->config[$key] : $default)
            : null;
    }
}