<?php

namespace App;

use App\Models\Film;
use App\Parsers\ParserInterface;

/**
 * Class HandleUrlLoggingDecorator
 * @package App
 */
class HandleUrlLoggingDecorator implements HandleUrlInterface
{
    /**
     * @var \App\HandleUrl
     */
    private $handler;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * HandleUrlLoggingDecorator constructor.
     *
     * @param \App\HandleUrl $handler
     */
    public function __construct(HandleUrl $handler)
    {
        $this->handler = $handler;
        $this->logger = Logger::getInstance();
    }

    /**
     * @param ParserInterface $parser
     * @param string $url
     * @return Film
     * @throws Exceptions\FilmNotFoundException
     */
    public function run(ParserInterface $parser, string $url): Film
    {
        $film = $this->handler->run($parser, $url);
        $this->logger->info('Parsed url: ' . $url);
        return $film;
    }
}