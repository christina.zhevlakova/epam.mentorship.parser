<?php

namespace App\Http;

/**
 * Interface HttpAdapterInterface
 * @package App\Http
 */
interface HttpAdapterInterface
{
    /**
     * @param string $url
     * @return string
     */
    public function getContent(string $url): string;
}
