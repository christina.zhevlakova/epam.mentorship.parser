<?php

namespace App\Http;

use App\Exceptions\ContentNotFoundException;
use GuzzleHttp\Client;

/**
 * Class GuzzleAdapter
 * @package App\Http
 */
class GuzzleAdapter implements HttpAdapterInterface
{
    /**
     * @var Client
     */
    private $adapter;

    /**
     * GuzzleAdapter constructor.
     */
    public function __construct()
    {
        $this->adapter = new Client;
    }

    /**
     * @param string $url
     * @return string
     * @throws ContentNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getContent(string $url): string
    {
        try{
            $response = $this->adapter->request('GET', $url);

            if ($response->getStatusCode() != 200) {
                throw new ContentNotFoundException;
            }
        }
        catch (\Exception $e) {
            throw new ContentNotFoundException;
        }

        return $response->getBody()->__toString();
    }
}