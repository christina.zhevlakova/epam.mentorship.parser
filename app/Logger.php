<?php

namespace App;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

/**
 * Class Logger
 * @package App
 */
class Logger extends AbstractLogger implements LoggerInterface
{
    const LOG_PATH = '../logs/parser.log';

    /**
     * @var
     */
    private static $instance;

    /**
     * Logger constructor.
     */
    private function __construct()
    {
        $file = fopen(self::LOG_PATH, 'w');
        fclose($file);
    }

    /**
     *
     */
    private function __clone() {}

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public static function getInstance(): LoggerInterface
    {
        if(!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     */
    public function log($level, $message, array $context = [])
    {
        error_log(
            date('Y-m-d H:i:s') . ' [' . ucfirst($level) . ']: ' . $message . "\r\n",
            3,
            self::LOG_PATH
        );
    }
}