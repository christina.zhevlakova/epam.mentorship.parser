<?php

namespace App;

use App\Models\Film;
use App\Parsers\ParserInterface;

/**
 * Interface HandleUrlInterface
 * @package App
 */
interface HandleUrlInterface
{
    /**
     * @param ParserInterface $parser
     * @param string $url
     * @return Film
     */
    public function run(ParserInterface $parser, string $url): Film;
}